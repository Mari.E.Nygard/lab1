package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 0;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + ++roundCounter);
            String humanChoice = userChoice();
            String computerChoice = computerChoice();

            // Check who won
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println("Human chose "+humanChoice+", computer chose " +computerChoice+". Human Wins!");
                humanScore ++;
            }
            else if (isWinner(computerChoice, humanChoice)) {
                System.out.println("Human chose "+humanChoice+", computer chose " +computerChoice+". Computer Wins!");
                computerScore ++;
            }
            else {
                System.out.println("Human chose "+humanChoice+", computer chose " +computerChoice+". It's a tie!");
            }
            System.out.println("Score: human " +humanScore+ ", computer " +computerScore);

            // Ask if human wants to play again
            String continueAnswer = continuePlaying();
            if (continueAnswer.contains("n")) {
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public String computerChoice() {
        Random rand = new Random();
        String computer = rpsChoices.get(rand.nextInt(rpsChoices.size()));      // selects random element from rps.Choices array
        return computer;
    }
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.contains("paper")) {
            return choice2.contains("rock");
        }
        else if (choice1.contains("rock")) {
            return choice2.contains("scissors");
        }
        else {
            return choice2.contains("paper");
        }
    }
    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(humanChoice)) {
                return humanChoice;
            }
            else {
                System.out.println("I do not understand" +humanChoice+". Could you try again?");
            }
        }
    }
    public String continuePlaying() {
        while (true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validateInput(continueAnswer)) {
                return continueAnswer;
            }
            else {
                System.out.println("I do not understand " +continueAnswer+ ". Try again");
            }
            
        }
    }
    public boolean validateInput(String readInput) {
        List<String> validInput = Arrays.asList("y", "n");
        return validInput.contains(readInput.toLowerCase()); 

    }

}
